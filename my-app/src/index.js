import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";

const root = ReactDOM.createRoot(document.getElementById("root"));

function changeTheme() {
  document.getElementsByTagName("body")[0].classList.toggle("dark");
}

root.render([
  React.createElement(
    "h1",
    { style: { color: "#999", fontSize: "19px" } },
    "Solar system planets"
  ),
  <label className="switch" htmlFor="checkbox">
    <input type="checkbox" id="checkbox" />
    <div className="slider round" onClick={changeTheme}></div>
  </label>,
  <ul className="planets-list">
    <li>Mercury</li>
    <li>Venus</li>
    <li>Earth</li>
    <li>Mars</li>
    <li>Jupiter</li>
    <li>Saturn</li>
    <li>Uranus</li>
    <li>Neptune</li>
  </ul>,
]);
